# building zamba

[zamba](https://zamba.hagiga.de) is my language agnostic language server; Free-software.

I would be happy if you would criticize me. Structuring (read: Decoupling) Scheme code appears to be hard given a single "environment". I am under the assumption that one can not use multiple binding-environments with Scheme.

One _DOES NOT_ need a installation of node.js (or npm etc.) to build zamba.
Node.js is required if one develops a custom langauge. Refer to [Dockerfile](#Dockerfile) if you want to _extend_ zamba. Otherwise, just raise an issue and I will extend (if possible) zamba with the language.

Languages need to have a grammar. Rudimentary support for the following langauges should be "simple"; Raise an issue and I will try my best to include them: [official supported parsers](https://tree-sitter.github.io/tree-sitter/#available-parsers).
Note that I need time to grasp the langauge; so that the requirements are satisfied..

# Dockerfile

The provided Dockerfile, which I run with `podman` instead enables one to _generate_ the `parser.c`-file.
This is only neccessary, if you want to _develop_ a grammar for tree-sitter. Like I did to provide [Scheme](https://gitlab.com/thchha/tree-sitter-scheme) support.

## Makefile

This file documents which commands I invoke to develop a grammar. This _is not required to use zamba_.

# build.sh

These are the *dirty* commands I used to compile the grammars for zamba. It is a mirror of my inexperience and dirty. It is a *reference* and not maintained.

