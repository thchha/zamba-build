FROM alpine:latest as git

# build environment
RUN apk add make
RUN apk add gcc
RUN apk add g++
RUN apk add git
RUN apk add cargo
RUN apk add npm

# make tree-sitter
RUN git clone https://github.com/tree-sitter/tree-sitter.git /opt/tree-sitter
WORKDIR /opt/tree-sitter
RUN make
RUN make install
WORKDIR /opt/tree-sitter/cli
RUN cargo build

ENV PATH="$PATH:/opt/tree-sitter/target/debug/"

WORKDIR /host/tree-sitter-lang/tree-sitter-scheme
CMD ["/bin/sh"]
