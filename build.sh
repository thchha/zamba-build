#!/bin/bash
# The entire content should be a Makefile, I am aware..
set -x

cd "$PWD/tree-sitter-java"
gcc -std=gnu99 -o libtree-sitter-java.so -I./src src/parser.c --shared -Os -fPIC -lstdc++
su -c "mv libtree-sitter-java.so /usr/local/lib/"
cd ..

cd "$PWD/tree-sitter-c"
gcc -std=gnu99 -o libtree-sitter-c.so -I./src src/parser.c --shared -Os -fPIC -lstdc++
su -c "mv libtree-sitter-c.so /usr/local/lib/"
cd ..

cd "$PWD/tree-sitter-scheme"
gcc -std=gnu99 -o libtree-sitter-scheme.so -I./src src/parser.c --shared -Os -fPIC -lstdc++
su -c "mv libtree-sitter-scheme.so /usr/local/lib/"
cd ..

cd "$PWD/tree-sitter-python"
gcc -I./src -std=gnu99 -O3 -Wall -Wextra -fPIC -c -o parser.o src/parser.c -lstdc++
g++ -fPIC -I./src -c -o scanner.o src/scanner.cc -lstdc++
g++ -shared -fPIC -Wl,-soname,libtree-sitter-python.so -o libtree-sitter-python.so parser.o scanner.o -lstdc++
rm parser.o
rm scanner.o
su -c "mv libtree-sitter-python.so /usr/local/lib/"
cd ..

# not supported.
cd "$PWD/tree-sitter-perl"
gcc -I./src -std=gnu99 -O3 -Wall -Wextra -fPIC -c -o parser.o src/parser.c -lstdc++
g++ -fPIC -I./src -c -o scanner.o src/scanner.cc -lstdc++
g++ -shared -fPIC -Wl,-soname,libtree-sitter-perl.so -o libtree-sitter-perl.so parser.o scanner.o -lstdc++
rm parser.o
rm scanner.o
su -c "mv libtree-sitter-perl.so /usr/local/lib/"
