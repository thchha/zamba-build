# build tree-sitter-grammers without using a local installation of node.js.
#
# This is MIT-Licenses; Just like tree-sitter.


all: build
	make run

build:
	podman build -t tree-sitter -f Dockerfile .

run:
	podman run -v $(PWD):/host -t -i tree-sitter tree-sitter generate

test:
	podman run -v $(PWD):/host -t -i tree-sitter tree-sitter test

clean:
	podman container prune -f
	podman image rm -f $(shell podman images --filter "dangling=true" -q --no-trunc)
